# F.E.M (Fast Erotic Mouse)


  The `GAYMOUSE` kernelmodule enables quake-live like acceleration for your mouse on Linux and is heavily inspired by previous works from [http://accel.drok-radnik.com/old.html](Povohat's mouse driver for Windows.)
  `GAYMOUSE` basically fuses the original [https://github.com/torvalds/linux/blob/master/drivers/hid/usbhid/usbmouse.c](Linux USB mouse driver) code with the acceleration code-base and has been initially developed by [https://github.com/chilliams](Christopher Williams).
  Since the [https://github.com/chilliams/mousedriver](original work) was in a rough state and seems to have been abandoned, therefore, this fork has been created.

  
### Installation
`./install`
   
### Uninstallation
`./uninstall`

### Manual compile, insmod, bind
 If you want to compile this module only for testing purposes or development, you do not need to install the whole package to your system

Compile the module, remove previously loaded modules and insert it.

``` sh
make clean && make
sudo rmmod leetmouse
sudo insmod ./driver/leetmouse.ko
```
If you did not install the udev rules before via `sudo make udev_install` you need to manually bind your mouse to this driver.

You can take a look at `/scripts/bind.sh` for an example on how to determine your mouse's USB address for that. However using the udev rules for development is advised.



## Shit I need to do!

| Task          | Progress      |    
| ------------- |:-------------:| 
| Implementing more variants of acceleration curves      | Doesn't seem all that hard. This will be the first significant thing I work on. | 
| Adding some kind of configuration method, like config files      | How the fuck do I do this?      |  
| Basic GUI, Realtime Graph, With Settings | Yeaah, gonna take a good minute for me to learn that shit but i'll try lmao      |  












  

                                    
